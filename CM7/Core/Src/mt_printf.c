/********************************************************************************/
/*                                                                              */
/*    OpenIPMC-FW                                                               */
/*    Copyright (C) 2020-2022 Andre Cascadan, Luigi Calligaris                  */
/*                                                                              */
/*    This program is free software: you can redistribute it and/or modify      */
/*    it under the terms of the GNU General Public License as published by      */
/*    the Free Software Foundation, either version 3 of the License, or         */
/*    (at your option) any later version.                                       */
/*                                                                              */
/*    This program is distributed in the hope that it will be useful,           */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*    GNU General Public License for more details.                              */
/*                                                                              */
/*    You should have received a copy of the GNU General Public License         */
/*    along with this program.  If not, see <https://www.gnu.org/licenses/>.    */
/*                                                                              */
/********************************************************************************/



/*
 * The following functions wrap printf and vprintf from printf submodule in order
 * to support their usage in multithread context.
 *
 * They follow the same argument pattern as the originals. Please check printf.h.
 *
 * It also implements printf with timestamping (good for logs and debugs).
 */


#include "FreeRTOS.h"
#include "semphr.h"
#include "main.h"
#include "printf.h"
#include "mt_printf.h"

// Mutex used to protect printf
static SemaphoreHandle_t printf_mutex = NULL;

// User RTC for timestamping
extern RTC_HandleTypeDef hrtc;


/*
 * Initializer.
 *
 * Better if called before start the scheduler (in main function)
 */
void mt_printf_init( void )
{
	printf_mutex = xSemaphoreCreateMutex();
}

int mt_printf(const char* format, ...)
{
	va_list args;

	va_start( args, format );
	int ret = mt_vprintf( format, args );
	va_end( args );

	return ret;
}

int mt_printf_tstamp(const char* format, ...)
{
	va_list args;

	va_start( args, format );
	int ret = mt_vprintf_tstamp( format, args );
	va_end( args );

	return ret;
}

int mt_vprintf(const char* format, va_list va)
{
	if( printf_mutex == NULL)
		return 0;

	// vprintf guarded by a mutex to prevent output conflict and string interleaving.
	xSemaphoreTake(printf_mutex, portMAX_DELAY);
	int ret = vprintf( format, va );
	xSemaphoreGive(printf_mutex);

	return ret;
}


int mt_vprintf_tstamp(const char* format, va_list va)
{
	if( printf_mutex == NULL)
		return 0;

	// Get time from RTC
	RTC_TimeTypeDef sTime = {0};
	RTC_DateTypeDef sDate = {0};
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);

	// vprintf guarded by a mutex to prevent output conflict and string interleaving.
	xSemaphoreTake(printf_mutex, portMAX_DELAY);
	printf("%.2d-%.2d-%.2d %.2d:%.2d:%.2d ",  sDate.Year, sDate.Month, sDate.Date , sTime.Hours, sTime.Minutes, sTime.Seconds);
	int ret = vprintf( format, va );
	xSemaphoreGive(printf_mutex);

	return ret;
}




